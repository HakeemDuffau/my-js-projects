import { elements, elementsStrings } from "./base";

export const getInput = () => elements.searchInput.value;

export const limitRecipeTitle = (title, limit = 17) => {
    const newTitle = [];
    if (title.length > limit) {
        title.split(" ").reduce((acc, curr) => {
            if (acc + curr.length <= limit) {
                newTitle.push(curr);
            }
            return acc + curr.length;
        }, 0);
        return `${newTitle.join(" ")} ...`;
    }

    return title;
};

const renderRecipe = (recipe) => {
    const markup = `<li>
                    <a class="results__link" href="#${recipe.recipe_id}">
                        <figure class="results__fig">
                            <img src="${recipe.image_url}" alt="Test">
                        </figure>
                        <div class="results__data">
                            <h4 class="results__name">${limitRecipeTitle(recipe.title)}</h4>
                            <p class="results__author">${recipe.publisher}</p>
                        </div>
                    </a>
                </li>`;
    elements.resultsList.insertAdjacentHTML("beforeend", markup);
};

export const clearInput = () => {
    elements.searchInput.value = "";
};

export const clearResults = () => {
    elements.resultsList.textContent = "";
};

const createButtons = (page, type) => `
    <button class="btn-inline results__btn--${type}" data-goto= ${type === "prev" ? page - 1 : page + 1}>
        <span>Page ${type === "prev" ? page - 1 : page + 1}</span>
        <svg class="search__icon">
            <use href="img/icons.svg#icon-triangle-${type === "prev" ? "left" : "right"}"></use>
        </svg>
    </button>
    `;

const renderButtons = (page, numResults, resultsPerPage) => {
    const numOfPages = Math.ceil(numResults / resultsPerPage);
    let button;

    if (page == 1 && numOfPages > 1) {
        //    Dispalay next Button
        button = createButtons(page, "next");
    } else if (page < numOfPages) {
        //    Display both
        button = `
            ${createButtons(page, "prev")}
            ${createButtons(page, "next")}
        `;
    } else if (page == numOfPages && numOfPages > 1) {
        //    Display Previous
        button = createButtons(page, "prev");
    }
    elements.resultsPages.insertAdjacentHTML("beforeend", button);
};

const clearPagination = () => {
    elements.resultsPages.textContent = "";
};
export const renderResults = (recipes, page = 2, resultsPerPage = 10) => {
    // Results
    const start = (page - 1) * resultsPerPage;
    const end = start + resultsPerPage;
    recipes.slice(start, end).forEach(renderRecipe);

    // Pagination
    clearPagination();
    renderButtons(page, recipes.length, resultsPerPage);
};

export const heightlightSelected = (id) => {
    document.querySelectorAll('.results__link').forEach((link) => {
        link.classList.remove(elementsStrings.resultLinkActive);
    })
    document.querySelector(`a[href="#${id}"]`).classList.add(elementsStrings.resultLinkActive);
};
