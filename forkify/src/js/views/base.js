export const elements = {
    searchInput: document.querySelector(".search__field"),
    searchForm: document.querySelector(".search"),
    resultsList: document.querySelector(".results__list"),
    results: document.querySelector(".results"),
    resultsPages: document.querySelector(".results__pages"),
    recipe: document.querySelector(".recipe"),
    shopingList: document.querySelector(".shopping__list"),
    LikesList: document.querySelector(".likes__list"),
    likesMenu: document.querySelector(".likes"),
};

export const elementsStrings = {
    loader: "loader",
    btnInline: "btn-inline",
    recipeIngredients: "recipe__ingredient-list",
    resultLinkActive: "results__link--active",
};

/* --------------------------------- Spinner -------------------------------- */

export const renderLoader = (parent) => {
    const loader = `
    <div class="${elementsStrings.loader}">
        <svg>
            <use href="img/icons.svg#icon-cw"></use>
        </svg>
    </div>
    `;

    parent.insertAdjacentHTML("afterbegin", loader);
};

export const clearLoader = () => {
    const loader = document.querySelector(`.${elementsStrings.loader}`);
    if (loader) loader.parentElement.removeChild(loader);
};
