import { elements } from "./base";
import {limitRecipeTitle} from './searchView';

export const addLike = (like) => {
    const markup = `<li>
                        <a class="likes__link" href="#${like.id}">
                            <figure class="likes__fig">
                                <img src="${like.img}" alt="Test">
                            </figure>
                            <div class="likes__data">
                                <h4 class="likes__name">${limitRecipeTitle(like.title)}</h4>
                                <p class="likes__author">${like.author}</p>
                            </div>
                        </a>
                    </li>`;
    elements.LikesList.insertAdjacentHTML("beforeend", markup);
};

export const deleteLike = (id) => {
    const like = document.querySelector(`.likes__list a[href="#${id}"]`);
    if (like) like.parentElement.removeChild(like);
};

export const toggleLikeButton = (isLiked) => {
    const icon = isLiked ? "icon-heart" : "icon-heart-outlined";
    document.querySelector(".recipe__love use").setAttribute("href", `img/icons.svg#${icon}`);
};

export const toggleLikeMenu = (numberOfLikes) => {
    elements.likesMenu.style.visibility =  numberOfLikes > 0 ? 'visible' : 'hidden';
};
