import { elements } from "./base";

export const renderItem = (item) => {
    const markup = `
    <li class="shopping__item" data-itemid = ${item.id}>
        <div class="shopping__count">
            <input type="number" value="${item.count}" step=${item.count} class="shopping__count-value">
            <p>${item.unit}</p>
        </div>
        <p class="shopping__description">${item.ingredient}</p>
        <button class="shopping__delete btn-tiny">
            <svg>
                <use href="img/icons.svg#icon-circle-with-cross"></use>
            </svg>
        </button>
    </li>`;
    elements.shopingList.insertAdjacentHTML("beforeend", markup);
};

export const deleteItem = (id) => {
    const item = Array.from(document.querySelectorAll(".shopping__item")).find((item) => item.dataset.itemid == id);
    if (item) {
        item.parentElement.removeChild(item);
    }
};

export const renderShopingList = (list) => {
    document.querySelector(".shopping__list").textContent = "";
    list.forEach((item) => {
        renderItem(item);
    });
};
