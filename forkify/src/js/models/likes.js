export default class Likes {
    constructor() {
        this.likes = [];
    }

    addLike(id, title, img, author) {
        const like = {
            id,
            title,
            img,
            author,
        };
        this.likes.push(like);

        // Persite data in local storage
        this.persisteData();

        return like;
    }

    deleteLike(id) {
        const index = this.likes.findIndex((like) => like.id == id);

        // Delete Item
        const deletedItem = this.likes.splice(index, 1)[0];

        // Persite data in local storage
        this.persisteData();

        return deletedItem;
    }

    isLiked(id) {
        return this.likes.findIndex((like) => like.id == id) > -1 ? true : false;
    }

    getNumberLike() {
        return this.likes.length;
    }

    persisteData() {
        localStorage.setItem("likes", JSON.stringify(this.likes));
    }

    readStorage() {
        const storage = localStorage.getItem("likes");
        if (storage) this.likes = JSON.parse(storage);
    }
}
