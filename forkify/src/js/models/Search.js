/* -------------------------------------------------------------------------- */
/*                                Search Model                                */
/* -------------------------------------------------------------------------- */
import axios from "axios";

export default class Search {
    constructor(query) {
        this.query = query;
        this.result = null;
    }

    async getResult() {
        try {
            const url = `https://forkify-api.herokuapp.com/api/search?q=${this.query}`;
            const result = await axios(url);
            this.result = result.data.recipes;
            // console.log(this.result);
        } catch (error) {
            alert(error);
        }
    }
}
