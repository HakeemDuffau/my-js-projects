/* ------------------------------ Recipe Model ------------------------------ */

import Axios from "axios";
import { elements } from "../views/base";

export default class Recipe {
    constructor(id) {
        this.id = id;
        this.title = "";
        this.img = "";
        this.author = "";
        this.url = "";
        this.ingredients = [];
        this.time = 0;
        this.servings = 0;
    }

    async getDetails() {
        try {
            const url = `https://forkify-api.herokuapp.com/api/get?rId=${this.id}`;
            const result = await Axios(url);
            if (result.status == 200) {
                const recipe = result.data.recipe;
                this.title = recipe.title;
                this.img = recipe.image_url;
                this.author = recipe.publisher;
                this.url = recipe.source_url;
                this.ingredients = recipe.ingredients;
            }

            //console.log(this);
        } catch (error) {
            alert(error);
        }
    }

    calcTime() {
        const numberOfIngs = this.ingredients.length;
        this.time = Math.ceil(numberOfIngs / 3) * 15;
    }

    calcServings() {
        this.servings = 4;
    }

    parseIngredients() {
        const unitsLong = ["tablespoons", "tablespoon", "ounces", "ounce", "teaspoons", "teaspoon", "cups", "pounds"];
        const unitsShort = ["tbsp", "tbsp", "oz", "oz", "tsp", "tsp", "cup", "pound"];
        const units = [...unitsShort, "kg", "g"];
        const newIngredients = this.ingredients.map((elmt) => {
            // 1. Uniform the units
            let ingredient = elmt.toLowerCase();

            unitsLong.forEach((unitLong, index) => {
                ingredient = ingredient.replace(unitLong, unitsShort[index]);
            });

            // 2. Remove Parentheses
            ingredient = ingredient.replace(/ *\([^)]*\) */g, " ");

            // 3. parse ingredients into count, unit, and ingredient
            const arrIng = ingredient.split(" ");
            const unitIndex = arrIng.findIndex((elmt2) => units.includes(elmt2));

            let objIng;
            if (unitIndex > -1) {
                const arrCount = arrIng.slice(0, unitIndex);
                let count;

                if (arrCount.length === 1) {
                    count = eval(arrIng[0].replace("-", "+"));
                } else {
                    count = eval(arrCount.join("+"));
                }

                objIng = {
                    count,
                    unit: arrIng[unitIndex],
                    ingredient: arrIng.slice(unitIndex + 1).join(" "),
                };
            } else if (parseInt(arrIng[0], 10)) {
                objIng = {
                    count: parseInt(arrIng[0], 10),
                    unit: "",
                    ingredient: arrIng.slice(1).join(" "),
                };
            } else if (unitIndex === -1) {
                objIng = {
                    count: 1,
                    unit: "",
                    ingredient: ingredient,
                };
            }
            return objIng;
        });

        this.ingredients = newIngredients;
    }

    updateServings(type) {
        // Update Servings
        const newSernings = type == "dec" ? this.servings - 1 : this.servings + 1;

        // Update Ingredients
        this.ingredients.forEach((ing) => {
            ing.count = (ing.count * newSernings) / this.servings;
        });

        this.servings = newSernings;
    }
}
