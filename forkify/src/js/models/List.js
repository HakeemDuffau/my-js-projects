import uniqid from "uniqid";
export default class List {
    constructor() {
        this.items = [];
    }

    addItem(count, unit, ingredient) {
        const item = {
            id: uniqid(),
            count,
            unit,
            ingredient,
        };
        this.items.push(item);
        return item;
    }

    removeItem(id) {
        if (id) {
            const index = this.items.findIndex((item) => item.id == id);
            if (index > -1) {
                this.items.splice(index, 1);
            }
        }
    }

    updateCount(id, newCount) {
        if (id) {
            this.items.find((item) => item.id == id).count = newCount;
        }
    }
}
