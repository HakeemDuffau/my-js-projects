import { elements, renderLoader, clearLoader, elementsStrings } from "./views/base";
import Search from "./models/Search";
import Recipe from "./models/Recipe";
import List from "./models/List";
import Likes from "./models/likes";
import * as searchView from "./views/searchView";
import * as recipeView from "./views/recipeView";
import * as listView from "./views/listView";
import * as likesView from "./views/likesView";

/*
Global state for our application
*Search OBject
*Current Recipe Object
*Shoping List Object
*Liked Recipes
*/
const state = {};

/* -------------------------------------------------------------------------- */
/*                              Search Controller                             */
/* -------------------------------------------------------------------------- */

const searchController = async () => {
    /* --------------------- 1. get the query from the view --------------------- */

    const query = searchView.getInput();

    if (query) {
        /* ---------------------- 2. create a new search object and add it to the state--------------------- */
        state.search = new Search(query);

        /* --------------------- 3. Prepare UI for the results -------------------- */
        searchView.clearInput();
        searchView.clearResults();
        renderLoader(elements.results);
        try {
            /* ---------------------------- 4. Search for recipes --------------------------- */
            await state.search.getResult();

            clearLoader();

            /* ------------------------- 5. Render Result On UI ------------------------- */
            searchView.renderResults(state.search.result);
        } catch (error) {
            alert("Something is going wrong in the Search controller");
        }
    }
};

/* -------------------------------------------------------------------------- */
/*                              Recipe COntroller                             */
/* -------------------------------------------------------------------------- */

const recipeController = async () => {
    const id = window.location.hash.substring(1);
    if (id) {
        // Prepare UI for change
        if (state.search) {
            searchView.heightlightSelected(id);
        }

        // Create new recipe object
        state.recipe = new Recipe(id);

        try {
            // Get recipe details & calculate time and servings
            recipeView.clearRecipe();
            renderLoader(elements.recipe);
            await state.recipe.getDetails();
            state.recipe.calcTime();
            state.recipe.calcServings();
            state.recipe.parseIngredients();
            clearLoader();

            // render recipe
            recipeView.renderRecipe(state.recipe, state.likes.isLiked(id));
        } catch (error) {
            console.log("error :>> ", error);
            alert("Something is going wrong in the Recipe controller");
        }
    }
};


const shoppingListController = () => {
    // Fill The shoping List
    if (!state.shopingList) {
        state.shopingList = new List();
    }
    state.recipe.ingredients.forEach((ing) => {
        state.shopingList.addItem(ing.count, ing.unit, ing.ingredient);
    });

    // Render SHoping List
    listView.renderShopingList(state.shopingList.items);
};

const LikesController = () => {
    // Check if the recipe is already liked
    if (!state.likes.isLiked(state.recipe.id)) {
        // Add a new Like Item
        const newLike = state.likes.addLike(state.recipe.id, state.recipe.title, state.recipe.img, state.recipe.author);

        // Toggle button Style
        likesView.toggleLikeButton(state.likes.isLiked(state.recipe.id));

        // Add Item to the view
        likesView.addLike(newLike);
    } else {
        // Delete the item from the likes list
        const deletedLike = state.likes.deleteLike(state.recipe.id);

        // Toggle button Style
        likesView.toggleLikeButton(state.likes.isLiked(state.recipe.id));

        // Delete item from the view
        likesView.deleteLike(deletedLike.id);
    }

    likesView.toggleLikeMenu(state.likes.getNumberLike());
};

/* -------------------------------------------------------------------------- */
/*                               Event Listener                               */
/* -------------------------------------------------------------------------- */

elements.searchForm.addEventListener("submit", (e) => {
    e.preventDefault();
    searchController();
});

elements.resultsPages.addEventListener("click", (e) => {
    const btn = e.target.closest(`.${elementsStrings.btnInline}`);
    if (btn) {
        const page = parseInt(btn.dataset.goto);
        searchView.clearResults();
        searchView.renderResults(state.search.result, page);
    }
});

["hashchange", "load"].forEach((event) => {
    window.addEventListener(event, recipeController);
});

window.addEventListener("load", (e) => {
    // Get liked recipe from the local storage
    state.likes = new Likes();
    state.likes.readStorage();

    // Render likes list
    state.likes.likes.forEach((like) => {
        likesView.addLike(like);
    });

    // Toggle like Menu
    likesView.toggleLikeMenu(state.likes.getNumberLike());
});

// Handling recipe botton clicks
elements.recipe.addEventListener("click", (e) => {
    if (e.target.matches(".btn-decrease, .btn-decrease *") && state.recipe.servings > 1) {
        state.recipe.updateServings("dec");
        recipeView.updateServingsIngredients(state.recipe);
    } else if (e.target.matches(".btn-increase, .btn-increase *")) {
        state.recipe.updateServings("inc");
        recipeView.updateServingsIngredients(state.recipe);
    } else if (e.target.matches(".recipe__btn, .recipe__btn *")) {
        shoppingListController();
    } else if (e.target.matches(".recipe__love, .recipe__love *")) {
        LikesController();
    }
});

// Handling shoping list button click
elements.shopingList.addEventListener("click", (e) => {
    const id = e.target.closest(".shopping__item").dataset.itemid;

    if (e.target.matches(".shopping__delete, .shopping__delete *")) {
        if (id) {
            // Remove the item from the model
            state.shopingList.removeItem(id);

            // Remove item from The view
            listView.deleteItem(id);
        }
    } else if (e.target.matches(".shopping__count-value, .shopping__count-value *")) {
        const newCount = parseFloat(e.target.value);
        if (newCount && id) {
            state.shopingList.updateCount(id, newCount);
        }
    }
});
