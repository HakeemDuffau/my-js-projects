/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

/**
 * Variables Definition
 */
var scores, roundScore, activePlayer, gamePlaying, previousRollDice;

init();

document.querySelector(".btn-roll").addEventListener("click", function () {
    if (gamePlaying) {
        //1. Generate a random Number between 1 and 6.
        var dice = Math.floor(Math.random() * 6) + 1;

        //2. Display the roll Number.
        diceDOM = document.querySelector(".dice");
        diceDOM.style.display = "block";
        diceDOM.src = "../img/dice-" + dice + ".png";

        //3. Update the round score if the rolled Number was NOT 1.
        if(dice === previousRollDice && dice === 6){
            scores[activePlayer] = 0;
            document.getElementById("score-"+activePlayer).textContent = "0";
            changeActivePlayer();
        } else if (dice !== 1) {
            roundScore += dice;
            document.getElementById("current-" + activePlayer).textContent = roundScore;
        } else {
            changeActivePlayer();
        }
        previousRollDice = dice;
    }
});

document.querySelector(".btn-hold").addEventListener("click", function () {
    if (gamePlaying) {
        //Add the round Score to the global score.
        scores[activePlayer] += roundScore;

        //Update the UI
        document.getElementById("score-" + activePlayer).textContent = scores[activePlayer];

        //Check if the player won the game
        if (scores[activePlayer] >= 100) {
            document.querySelector(".player-" + activePlayer + "-panel").classList.add("winner");
            document.getElementById("name-" + activePlayer).textContent = "Winner";
            document.querySelector(".dice").style.display = "none";
            document.querySelector(".player-" + activePlayer + "-panel").classList.remove("active");
            gamePlaying = false;
        } else {
            changeActivePlayer();
        }
    }
});

document.querySelector(".btn-new").addEventListener("click", init);

function changeActivePlayer() {
    roundScore = 0;
    activePlayer = activePlayer == 0 ? 1 : 0;

    document.getElementById("current-0").textContent = "0";
    document.getElementById("current-1").textContent = "0";

    document.querySelector(".player-0-panel").classList.toggle("active");
    document.querySelector(".player-1-panel").classList.toggle("active");

    document.querySelector(".dice").style.display = "none";
}

function init() {
    scores = [0, 0];
    roundScore = 0;
    activePlayer = 0;

    document.querySelector(".dice").style.display = "none";

    document.getElementById("score-0").textContent = "0";
    document.getElementById("score-1").textContent = "0";
    document.getElementById("current-0").textContent = "0";
    document.getElementById("current-0").textContent = "0";

    document.querySelector(".player-0-panel").classList.remove("active", "winner");
    document.querySelector(".player-1-panel").classList.remove("active", "winner");
    document.querySelector(".player-0-panel").classList.add("active");

    document.getElementById("name-0").textContent = "Player 1";
    document.getElementById("name-1").textContent = "Player 2";
    gamePlaying = true;
}
