/* -------------------------------------------------------------------------- */
/*                           Asynchronous Javascript                          */
/* -------------------------------------------------------------------------- */
/*const second = () => {
    setTimeout(() => {
        console.log("Async - Hey there !");
    }, 2000);
};
const first = () => {
    console.log("Hey there !");
    second();
    console.log("The end");
};

first();*/

/* -------------------- Asynch JS With CallBack Functions -------------------- */
/*
function getRecipe() {
    setTimeout(() => {
        const recipeIds = [881, 786, 3637, 6363, 5636];
        console.log(recipeIds);
        setTimeout(
            (id) => {
                const recipe = {
                    title: "Tomato Pasta",
                    publisher: "Hakeem",
                };
                console.log(`${id} : ${recipe.title}`);

                setTimeout((publisher) => {
                    console.log(`${publisher} : ${recipeIds[3]}, ${recipeIds[4]}`)
                }, 1500, recipe.publisher);
            },
            1500,
            recipeIds[2]
        );
    }, 1500);
}

getRecipe();
*/

/* -------------------- Asynch JS With Promise-------------------- */
/**
 * Promise : Pending --> Resolved ==> FullFilled or Rejected
 */
const getIds = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve([881, 786, 3637, 6363, 5636]);
    }, 1500);
});

const getRecipe = (RecID) => {
    return new Promise((resolve, reject) => {
        setTimeout(
            (id) => {
                resolve({
                    id: id,
                    title: "Tomato & Salade",
                    publisher: "Hakeem",
                });
            },
            1500,
            RecID
        );
    });
};

const getRelated = (publisher) => {
    return new Promise((resolve, reject) => {
        setTimeout(
            (publisher) => {
                resolve(`${publisher} : Sauce Poulet, Pizza`);
            },
            1500,
            publisher
        );
    });
};
/*
getIds
    .then((result) => {
        console.log(result);
        return getRecipe(result[2]);
    })
    .then((recipe) => {
        console.log(recipe);
        return getRelated(recipe.publisher);
    })
    .then((related) => {
        console.log(related);
    })
    .catch((error) => {
        console.log(error);
    });
*/

/* -------------------------------------------------------------------------- */
/*                                Async / Await                               */
/* -------------------------------------------------------------------------- */
/*
async function getRecipesAW() {
    const IDs = await getIds;
    console.log(IDs);
    const recipe = await getRecipe(IDs);
    console.log(recipe);
    const related = await getRelated(recipe.publisher);
    console.log(related);

    return recipe;
}

getRecipesAW().then((result) => {
    console.log(`Recipe : ${result.title}`);
})

*/

/* -------------------------------------------------------------------------- */
/*                            Ajax Fetch & Promise                            */
/* -------------------------------------------------------------------------- */
const getUser = (id) => {
    fetch("https://jsonplaceholder.typicode.com/users")
        .then((result) => {
            console.log(result);
            return result.json();
        })
        .then((data) => {
            const user = data[id];
            console.log(`${user.id} : My name is ${user.name}, here is my email ${user.email}`);
        })
        .catch((error) => {
            console.log(error);
        });
};
/*
getUser(7);
getUser(2);
getUser(3);
*/

/* -------------------------------------------------------------------------- */
/*                             Ajax Async / Await                             */
/* -------------------------------------------------------------------------- */

async function getPost(id) {
    try {
        const result = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
        const post = await result.json();
        console.log(`Title : ${post.title} \n Text : ${post.body} \n Author ID : ${post.userId}`);
        return post;
    } catch (error) {
        console.log(error);
    }
}

getPost(1).then((post) => {
    console.log(post);
});
