/* -------------------------------------------------------------------------- */
/*                               Infrastructure                               */
/* -------------------------------------------------------------------------- */

class Infrastructure {
    constructor(name, buildYear) {
        this.name = name;
        this.buildYear = buildYear;
    }

    calculateAge() {
        return new Date().getFullYear() - this.buildYear;
    }
}

/* -------------------------------------------------------------------------- */
/*                                    Park                                    */
/* -------------------------------------------------------------------------- */

class Park extends Infrastructure {
    constructor(name, buildYear, numberOfTrees, area) {
        super(name, buildYear);
        this.numberOfTrees = numberOfTrees;
        this.area = area;
    }

    calculateTreeDensity() {
        return Math.round(this.numberOfTrees / this.area);
    }

    displayTreeDensity() {
        console.log(
            `${this.name} has a tree density of ${this.calculateTreeDensity()} trees per square Km.`
        );
    }
}

/* -------------------------------------------------------------------------- */
/*                                   Street                                   */
/* -------------------------------------------------------------------------- */

class Street extends Infrastructure {
    constructor(name, buildYear, length, size = "Normal") {
        super(name, buildYear);
        this.length = length;
        this.size = size;
    }
}

/* -------------------------------------------------------------------------- */
/*                               Data Structures                              */
/* -------------------------------------------------------------------------- */
let parks, streets;
parks = new Array();
streets = new Array();

parks.push(new Park("Park 1", 1998, 400, 5));
parks.push(new Park("Park 2", 1990, 800, 7));
parks.push(new Park("Park 3", 1998, 1000, 3));

streets.push(new Street("Street 1", 2000, 10));
streets.push(new Street("Street 1", 1998, 30, "Small"));
streets.push(new Street("Street 1", 1991, 40, "Big"));
streets.push(new Street("Street 1", 1990, 8, "Huge"));

/* -------------------------------------------------------------------------- */
/*                                   Reports                                  */
/* -------------------------------------------------------------------------- */

console.log(" ------------------------------ Parks Report ------------------------------");

/* ---------------------- 1. Tree density of each park ---------------------- */

parks.forEach((park) => park.displayTreeDensity());

/* --------------------- 2. The average age of the parks -------------------- */
let sumAge, average;
sumAge = parks.reduce((prev, curr, index) => prev + curr.calculateAge(), 0);

average = sumAge / parks.length;

console.log(`Our ${parks.length} parks have an average age of ${average} years.`);

/* ------------- 3. Name of Parks That have more than 1000 trees ------------- */

parks
    .filter((park) => park.numberOfTrees >= 1000)
    .forEach((park) => console.log(`${park.name} has more than 1000 trees.`));
