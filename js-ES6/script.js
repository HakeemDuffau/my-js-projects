/* -------------------------------------------------------------------------- */
/*                                Let and const                               */
/* -------------------------------------------------------------------------- */

/* ----------------------------------- ES5 ---------------------------------- */

var name5 = "Jhone Smith";
var age5 = 24;
var name5 = "Liam Terner";

//console.log(name5);

/* ----------------------------------- ES6 ---------------------------------- */
const name6 = "Jhone Smith";
//name6 = 'Liam Termner'; Error!!
//console.log(name6);

/* -------------------------------------------------------------------------- */
/*                               Driver Licence                               */
/* -------------------------------------------------------------------------- */

/* ----------------------------------- ES5 ---------------------------------- */

function driverLicence5(passTest) {
    if (passTest) {
        console.log(firstName); //undefined because of hosting
        var firstName = "Jhone Andrew";
        var yearOfBirth = 1997;
    }
    console.log(
        "ES5 : " +
            firstName +
            " born in " +
            yearOfBirth +
            "is now officially allowed to drive a care."
    );
}

//driverLicence5(true);

/* ----------------------------------- ES6 ---------------------------------- */
function driverLicence6(passTest) {
    //console.log(firstName); Error no Hosting here;
    let firstName;
    const yearOfBirth = 1997;
    if (passTest) {
        firstName = "Jhone Andrew";
    }
    console.log(
        "ES6 : " +
            firstName +
            " born in " +
            yearOfBirth +
            " is now officially allowed to drive a care."
    );
}

//driverLicence6(true);

/* -------------------------------------------------------------------------- */
/*                IIFE : Les fonctions immédiatement executées                */
/* -------------------------------------------------------------------------- */

/* ----------------------------------- ES5 ---------------------------------- */

(function () {})();

/* ----------------------------------- ES6 ---------------------------------- */
{
    let a;
    a = 10;
    let test = function () {
        console.log("Hello");
    };
}

/* -------------------------------------------------------------------------- */
/*                                   Strings                                  */
/* -------------------------------------------------------------------------- */

let firstName = "Jhone";
let lastName = "Smith";
const yearOfBirth = 1996;

function calcAge() {
    return 2020 - yearOfBirth;
}

/* ----------------------------------- ES5 ---------------------------------- */
/*console.log(
    "ES5 : This is " +
        firstName +
        " " +
        lastName +
        " he's " +
        calcAge() +
        " years old."
);*/

/* ----------------------------------- ES6 ---------------------------------- */
/*console.log(
    `ES6: this is ${firstName} ${lastName} and he's ${calcAge()} years old.`
);*/

const fullName = `${firstName} ${lastName}`;
//console.log(fullName.startsWith("J"));

/* -------------------------------------------------------------------------- */
/*                               Arrow Function                               */
/* -------------------------------------------------------------------------- */

let arr = [2, 4, 6];
arr = arr.map((val, i, array) => val / 2);
//console.log(`Tableau Final : ${arr}`);

/* -------------------------------------------------------------------------- */
/*                                Destructuring                               */
/* -------------------------------------------------------------------------- */

Jhone = ["Jhone", 1997];

const [name, age] = Jhone;
//console.log(`Name : ${name} Age : ${age}`);

let a = 10;
let b = 20;

[a, b] = [b, a];
//console.log(`A = ${a} and B = ${b}`);

const obj = {
    firstName: "Hakim",
    lastName: "Duffau",
};

let { firstName: c, lastName: d } = obj;
//console.log(`${c} ${d}`);

/* -------------------------------------------------------------------------- */
/*                                   Arrays                                   */
/* -------------------------------------------------------------------------- */

const box = document.querySelectorAll(".box");
//console.log("box :>> ", box);

/* ----------------------------------- ES5 ---------------------------------- */
/*var boxArray5 = Array.prototype.slice.call(box);
console.log('boxArray5 :>> ', boxArray5);

boxArray5.forEach(element => {
    element.style.backgroundColor= 'blue'
});*/

/* ----------------------------------- ES6 ---------------------------------- */

Array.from(box).forEach((element) => {
    //element.style.backgroundColor = "blue";
});

let tab = ["Hello", "I", "am", "Hakim", "DUffau"];

for (const element of tab) {
    //console.log('element :>> ', element);
}

let element = tab.filter((element) => element.includes("ello"));
//console.log('element :>> ', element);

/* -------------------------------------------------------------------------- */
/*                                    Maps                                    */
/* -------------------------------------------------------------------------- */

const question = new Map();

question.set("number", 23);
//console.log('question :>> ', question);

/* -------------------------------------------------------------------------- */
/*                                    Class                                   */
/* -------------------------------------------------------------------------- */

/* ----------------------------------- ES5 ---------------------------------- */

var Person5 = function (firstName, lastName, yearOfBirth) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.yearOfBirth = yearOfBirth;
};

Person5.prototype.calculateAge = function () {
    return new Date().getFullYear() - this.yearOfBirth;
};

var Athlete5 = function (firstName, lastName, yearOfBirth, sport) {
    Person5.call(this, firstName, lastName, yearOfBirth);
    this.sport = sport;
};

Athlete5.prototype = Object.create(Person5.prototype);

var athlete5 = new Athlete5("Hakeem", "DUffau", 1996, "Footballeur");

//console.log("athlete5 :>> ", athlete5);
//console.log("athlete5.calculateAge() :>> ", athlete5.calculateAge());

/* ----------------------------------- ES6 ---------------------------------- */

class Person6 {
    constructor(firstName, lastName, yearOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
    }

    calculateAge() {
        return new Date().getFullYear() - this.yearOfBirth;
    }

    static test = 0;
}

class Athlete6 extends Person6 {
    constructor(firstName, lastName, yearOfBirth, sport) {
        super(firstName, lastName, yearOfBirth);
        this.sport = sport;
    }

    displayAge() {
        console.log(`Age ${this.calculateAge()}`);
    }
}

let athlete6 = new Athlete6("Hakeem", "DUffau", 1996, "Footballeur");

athlete6.displayAge();
