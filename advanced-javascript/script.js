// Burning Questions Game
(function (params) {
    var questions, score;

    function Question(question, possibleAwswers, correctAnswer) {
        this.question = question;
        this.possibleAwswers = possibleAwswers;
        this.correctAnswer = correctAnswer;
    }

    Question.prototype.ask = function () {
        console.log(this.question);
        console.log("______________________________________");
        for (var i = 0; i < this.possibleAwswers.length; i++) {
            console.log(i + 1 + ") " + this.possibleAwswers[i]);
        }
    };

    Question.prototype.checkAnswer = function checkResponse(answer) {
        return this.correctAnswer == answer;
    };

    // 2. Creating a couple of questions
    questions = [];
    questions.push(
        new Question(
            "Is Javascript the coolest programing language in the world ?",
            ["Yes", "No"],
            1
        )
    );
    questions.push(
        new Question(
            "What is the name of this course's teacher",
            ["Fama", "Jean", "Emily", "Jonas"],
            4
        )
    );
    questions.push(
        new Question("What is the difficulty level of this course ?", ["Hard", "Medium", "Low"], 2)
    );

    

    function score() {
        var sc = 0;
        return function (isAnswerCorrect) {
            if(isAnswerCorrect){
                sc++;
            }
            return sc;
        }
    }

    var keepscore = score();
    function nextQuestion() {
        var sc;
        var question = questions[Math.floor(Math.random() * questions.length)];
        question.ask();
        var answer = prompt("Please correct the correct answer :");
        if (answer != "exit") {
            var isAnswerCorrect = question.checkAnswer(answer);
            if (isAnswerCorrect) {
                console.log("Correct answer!");
                sc = keepscore(true);
            } else {
                console.error("Wrong answer, Please try again!");
            }
            console.log("Score : " + sc);
            nextQuestion();
        } else {
            console.log("Bye!");
        }
    }
    nextQuestion();
})();
