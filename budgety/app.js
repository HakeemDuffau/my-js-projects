/* -------------------------------------------------------------------------- */
/*                              Budget Controller                             */
/* -------------------------------------------------------------------------- */

var budgetController = (function () {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    Expense.prototype.percentage = -1;

    var data = {
        allItems: {
            inc: [],
            exp: [],
        },

        totals: {
            inc: 0,
            exp: 0,
        },
        expensesPercentage: -1,
        budget: 0,
    };

    var addItem = function (type, description, value) {
        var newItem, ID;

        /* -------------------------- Determine the next id ------------------------- */

        ID =
            data.allItems[type].length > 0
                ? data.allItems[type][data.allItems[type].length - 1].id + 1
                : 0;

        /* --------------------------- Create the new item -------------------------- */

        if (type === "inc") {
            var newItem = new Income(ID, description, value);
        } else if (type === "exp") {
            var newItem = new Expense(ID, description, value);
        }

        /* ----------------- Add the new item to our data stracture ----------------- */

        data.allItems[type].push(newItem);
        data.totals[type] += newItem.value;
        ID++;

        /* ----------------------- Return the new created item ---------------------- */
        return newItem;
    };

    var deleteItem = function (type, id) {
        var idsArr, index, itemToDelete;
        idsArr = data.allItems[type].map(function (item) {
            return item.id;
        });
        console.log(idsArr);
        index = idsArr.indexOf(id);
        if (index != -1) {
            data.totals[type] -= data.allItems[type][index].value;
            data.allItems[type].splice(index, 1);
        }
    };

    var calculateBudget = function () {
        data.budget = data.totals.inc - data.totals.exp;
        if (data.totals.inc != 0) {
            data.expensesPercentage = Math.round((data.totals.exp / data.totals.inc) * 100);
        } else {
            data.expensesPercentage = -1;
        }
    };

    var getBudget = function () {
        return {
            totalInc: data.totals.inc,
            totalExp: data.totals.exp,
            budget: data.budget,
            expensesPercentage: data.expensesPercentage,
        };
    };

    var calculatePercentages = function () {
        data.allItems.exp.forEach(function (item) {
            item.percentage =
                data.totals.inc > 0 ? Math.round((item.value / data.totals.inc) * 100) : -1;
        });
    };

    var getPercentages = function () {
        return data.allItems.exp.map(function (item) {
            return {
                id: item.id,
                percentage: item.percentage,
            };
        });
    };

    var getCurrentDate = function () {
        var date, month, year, allMonths;
        allMonths = [
            "Janvier",
            "Fevrier",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Decembre",
        ];
        date = new Date();
        month = allMonths[date.getMonth()];
        year = date.getFullYear();

        return {
            month: month,
            year: year,
        };
    };

    return {
        addItem: addItem,
        deleteItem: deleteItem,
        calculateBudget: calculateBudget,
        calculatePercentages: calculatePercentages,
        getBudget: getBudget,
        getPercentages: getPercentages,
        getCurrentDate: getCurrentDate,
    };
})();

/* -------------------------------------------------------------------------- */
/*                                UI Controller                               */
/* -------------------------------------------------------------------------- */

var UIController = (function () {
    var DOMStrings = {
        imputType: ".add__type",
        inputDescription: ".add__description",
        inputValue: ".add__value",
        addButton: ".add__btn",
        budgetValue: ".budget__value",
        budgetIncomeValue: ".budget__income--value",
        budgetIncomePercentage: ".budget__income--percentage",
        budgetExpensesValue: ".budget__expenses--value",
        budgetExpensesPercentage: ".budget__expenses--percentage",
        incomeList: ".income__list",
        expensesList: ".expenses__list",
        container: ".container",
        expensePercentage: "#exp-%id% .item__percentage",
        currentDate: ".budget__title--month",
    };

    function getImput() {
        return {
            addType: document.querySelector(DOMStrings.imputType).value,
            addDescription: document.querySelector(DOMStrings.inputDescription).value,
            addValue: Math.abs(parseFloat(document.querySelector(DOMStrings.inputValue).value)),
        };
    }

    function addListItem(obj, type) {
        var htm, newHtml, list;
        if (type === "inc") {
            htm =
                '<div class="item clearfix" id="inc-%id%">' +
                '<div class="item__description">%description%</div>' +
                '<div class="right clearfix">' +
                '<div class="item__value">+ %value%</div>' +
                '<div class="item__delete">' +
                '<button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>' +
                "</div>" +
                "</div>" +
                "</div>";
            list = DOMStrings.incomeList;
        } else if (type === "exp") {
            htm =
                '<div class="item clearfix" id="exp-%id%">' +
                '<div class="item__description">%description%</div>' +
                '<div class="right clearfix">' +
                '<div class="item__value">- %value%</div>' +
                '<div class="item__percentage">10%</div>' +
                '<div class="item__delete">' +
                '<button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>' +
                "</div>" +
                "</div>" +
                "</div>";
            list = DOMStrings.expensesList;
        }

        newHtml = htm.replace("%id%", obj.id);
        newHtml = newHtml.replace("%description%", obj.description);
        newHtml = newHtml.replace("%value%", obj.value);

        document.querySelector(list).insertAdjacentHTML("beforebegin", newHtml);
    }

    function deleteListItem(id) {
        document.getElementById(id).remove();
    }

    function clearInpuFields() {
        var fields, fieldsArr;
        fields = document.querySelectorAll(
            DOMStrings.inputDescription + ", " + DOMStrings.inputValue
        );
        fieldsArr = Array.prototype.slice.call(fields);
        fieldsArr.forEach(function (element) {
            element.value = "";
        });
        fieldsArr[0].focus();
    }

    function updateBudgetUI(budgetData) {
        document.querySelector(DOMStrings.budgetValue).textContent = budgetData.budget;
        document.querySelector(DOMStrings.budgetIncomeValue).textContent = budgetData.totalInc;
        document.querySelector(DOMStrings.budgetExpensesValue).textContent = budgetData.totalExp;
        document.querySelector(DOMStrings.budgetExpensesPercentage).textContent =
            budgetData.expensesPercentage > 0 ? budgetData.expensesPercentage + "%" : "---";
    }

    function updatePercentagesUI(percentagesList) {
        percentagesList.forEach(function (item) {
            console.log(DOMStrings.expensePercentage.replace("%id%", item.id));
            if (percentagesList.length > 0) {
                document.querySelector(
                    DOMStrings.expensePercentage.replace("%id%", item.id)
                ).textContent = item.percentage > 0 ? item.percentage + "%" : "--";
            }
        });
    }

    function formatNumer(num, type) {
        var numSplit, int, dec;

        num = Math.abs(num);
        num = num.toFixed(2);

        numSplit = num.split(".");
        int = numSplit[0];
        dec = numSplit[1];
    }

    function displayCurrentDate(date) {
        document.querySelector(DOMStrings.currentDate).textContent =
            date.month + " " + date.year + " ";
    }

    function changeType() {
        var type, description, value, addButton;
        type = document.querySelector(DOMStrings.imputType);
        description = document.querySelector(DOMStrings.inputDescription);
        value = document.querySelector(DOMStrings.inputValue);
        addButton = document.querySelector(DOMStrings.addButton);

        type.classList.toggle("red-focus");
        description.classList.toggle("red-focus");
        value.classList.toggle("red-focus");
        addButton.classList.toggle("red");

        console.log("Changed!!");
    }

    return {
        getDOMStrings: DOMStrings,
        getImput: getImput,
        addListItem: addListItem,
        deleteListItem: deleteListItem,
        clearInpuFields: clearInpuFields,
        updateBudgetUI: updateBudgetUI,
        updatePercentagesUI: updatePercentagesUI,
        displayCurrentDate: displayCurrentDate,
        changeType: changeType,
    };
})();

/* -------------------------------------------------------------------------- */
/*                            Global App Controller                           */
/* -------------------------------------------------------------------------- */

var controller = (function (budgetCtrl, UICtrl) {
    var setUpEventListeners = function () {
        var DOM = UICtrl.getDOMStrings;

        /* ----------------------------- OK Button Event ---------------------------- */

        document.querySelector(DOM.addButton).addEventListener("click", ctrlAddItem);

        document.addEventListener("keypress", function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });

        /* --------------------------- Delete Button Event -------------------------- */
        document.querySelector(DOM.container).addEventListener("click", ctrDeleteItem);

        document.querySelector(DOM.imputType).addEventListener("change", UICtrl.changeType);
    };

    var ctrlAddItem = function () {
        var input, newItem;
        /* ----------------------- 1. Get the field input data ---------------------- */

        input = UICtrl.getImput();
        if (input.addValue && input.addDescription) {
            /* -------------- 2. Add new item to our budget datastructure --------------- */
            newItem = budgetCtrl.addItem(input.addType, input.addDescription, input.addValue);

            /* ------------------------ 3. Add new Item to the UI ----------------------- */
            UICtrl.addListItem(newItem, input.addType);
            UICtrl.clearInpuFields();

            /* --------------------- 4. Calculate Budget and Update UI ------------------- */
            updateBudget();

            /* --------------------------- Update Percentages --------------------------- */
            updatePercentages();
        }
    };

    var updateBudget = function () {
        var budgetData;
        /* --------------------------- Calculate Budget ----------------------------- */
        budgetCtrl.calculateBudget();
        /* ------------------------------ Return Budget ----------------------------- */
        budgetData = budgetCtrl.getBudget();
        /* -------------------------------- Update UI ------------------------------- */
        UICtrl.updateBudgetUI(budgetData);
    };

    var updatePercentages = function () {
        var percentagesList;
        /* -------------------------- Calculate Percentages ------------------------- */
        budgetCtrl.calculatePercentages();

        /* --------------------------- Return percentages --------------------------- */
        percentagesList = budgetCtrl.getPercentages();

        /* --------------------------- Update Percentages UI--------------------------- */
        UICtrl.updatePercentagesUI(percentagesList);
    };

    var ctrDeleteItem = function (event) {
        var itemID, splitID, id, type;
        itemID = parentNodeAt(event.target, 4).id;
        if (itemID) {
            splitID = itemID.split("-");
            id = parseInt(splitID[1]);
            type = splitID[0];
            console.log(type + "    " + id);

            /* -------------------- Delete Item from the budget dats -------------------- */
            budgetCtrl.deleteItem(type, id);

            /* ------------------------- Delete Item from the UI ------------------------ */
            UICtrl.deleteListItem(itemID);

            /* ---------------------------- calculate & Update Budget ---------------------------- */
            updateBudget();

            /* --------------------------- Update Percentages --------------------------- */
            updatePercentages();
        }
    };

    var ctrlDisplayCurrentDay = function () {
        UICtrl.displayCurrentDate(budgetCtrl.getCurrentDate());
    };

    function parentNodeAt(node, level) {
        var parent = node;
        for (let i = 0; i < level; i++) {
            parent = parent.parentNode;
        }
        return parent;
    }

    return {
        init: function () {
            console.log("The application has started.");
            ctrlDisplayCurrentDay();
            setUpEventListeners();
            UICtrl.updateBudgetUI(budgetCtrl.getBudget());
        },
    };
})(budgetController, UIController);

controller.init();
